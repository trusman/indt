<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>INDT Test</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">

        <link href="css/custom-css.css" rel="stylesheet">
    </head>
    <body>
        <h2 class="text-center">List of Authors</h2>

        <div class="container">

            <form action="/create" method="POST" enctype="multipart/form-data">
          
              {{ csrf_field() }}

              <div class="form-group">
                <label>Upload file: </label>
                <label class="custom-file">
                  <input type="file" id="file" name="file" class="custom-file-input">
                  <span class="custom-file-control"></span>
                </label>

                <button type="submit" class="btn btn-primary">Send</button>
                <a href="/download-file" class="align-right">Download file</a>
              </div>
            </form>
        </div>

        <main role="main" class="container">
            <div class="flex-center position-ref full-height">
                <div class="content">
                    <div id="accordion" role="tablist">
                        @foreach ($authors as $author)
                          <div class="card">
                            <div class="card-header" role="tab" id="headingOne">
                              <h5 class="mb-0">
                                <a data-toggle="collapse" href="#collapse{{$author['id']}}" aria-expanded="true" aria-controls="collapse{{$author['id']}}">
                                  {{ $author['firstName'] . ' ' . $author['lastName'] }}
                                </a>
                                <a href="{{ url('/delete-author', ['id' => $author['id']]) }}" class="align-right"><i class="fa fa-trash" aria-hidden="true"></i></a>
                              </h5>
                            </div>

                            <div id="collapse{{$author['id']}}" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                              <div class="card-body">
                                <ul class="list-group">
                                    @foreach ($author['books'] as $book)
                                      <li class="list-group-item d-flex justify-content-between align-items-center">
                                        {{ trim($book['title']) }}
                                        <a href="{{ url('/delete-book', ['id' => $book['id']]) }}"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                      </li>
                                    @endforeach
                                </ul>
                              </div>
                            </div>
                          </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </main>     

        <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="js/custom-js.js"></script>
       
        @if(Session::has('message')) 
            <script type="text/javascript">
                swal("","{{ Session::get('message') }}",'success');
            </script>
        @endif

        @if(Session::has('error')) 
            <script type="text/javascript">
                swal("","{{ Session::get('error') }}",'error');
            </script>
        @endif
       
    </body>
</html>
