## Pré-requisitos
	- PHP 7.1
	- Apache2
	- SO linux

## Formato arquivo

	- id Author|firstName|lastName|id Book|title
	- Para inserir um autor com livro: |JRR|Martin||Game of thrones
	- Para inserir um autor: |JRR|Martin||
	- Para inserir um livro para um autor existente: 1|JRR|Martin||Novo livro

## Instruções para rodar o teste

- [Baixar o repositório aqui](https://bitbucket.org/trusman/indt/downloads/).
- Extrair o arquivo baixado e renomear a pasta para indt.
- Entrar na pasta indt via terminal e rodar o servidor com o comando: **php artisan serve**.
- Acessar [localhost](http://localhost:8000/).

## Formato arquivo de importação

- id Author|firstName|lastName|id Book|title
- Para inserir um autor com livro: |JRR|Martin||Game of thrones
- Para inserir um autor: |JRR|Martin||
- Para inserir um livro para um autor existente:1|JRR|Martin||Novo livro
