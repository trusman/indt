<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ApiController@index')->name('home');

Route::get('/download-file', 'ApiController@generateFileExemple');

Route::post('/create', 'ApiController@store');

Route::get('/delete-book/{id}', 'ApiController@deleteBook');

Route::get('/delete-author/{id}', 'ApiController@deleteAuthor');
