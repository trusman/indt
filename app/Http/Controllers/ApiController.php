<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class ApiController extends Controller
{
	const STATUS_OK = 200;
	const FILENAME = 'arquivo_exemplo1.txt';
	const MIME_TYPE = 'text/plain';

	public function index()
	{
		$authors = $this->getAllDataByAuthors();
			
		return view('welcome', compact('authors'));		
	}

	/**
	* Function to store authors and its respectives books
	*/
	public function store(Request $request)
	{
		try {

			$file = $request->file('file');

			if ($file != null) {
				
				if ($file->getMimeType() == self::MIME_TYPE) {
					$jsonData = $this->changeFileToJson($file);

					$client = new Client();
					$client->request('POST', 'https://bibliapp.herokuapp.com/api/books', [
						'headers'  => ['content-type' => 'application/json'],
						'body' => $jsonData['books']
					]);			

					session()->flash('message', 'Files registered with successful');	

				} else {
					session()->flash('error', 'Only text file is permited');
				}			

			} else {
				session()->flash('error', 'Please select a file');			
			}

		} catch(Exception $e) {
			session()->flash('error', $e->getMessage());
			//'Unknown error'
		}		

		return redirect()->route('home');
	}

	/**
	* Function to change data from file in json data
	*/
	private function changeFileToJson($file)
	{
		$handle = @fopen($file, "r");
		if ($handle) {
			$data = array();
		    while (($buffer = fgets($handle, 4096)) !== false) {
		    	
		    	if (!empty(trim($buffer))) 
		        	array_push($data, explode("|", $buffer));
		    }

		    if (!feof($handle)) {
		    	session()->flash('error', 'fgets fail');
		        
		    }

		    fclose($handle);
		}

		$arrayAuthors = array();
		$arrayBooks   = array();
		foreach ($data as $key => $value) {

			if ($key != 0) {

				$authorExist = !empty($value[0]) ? $this->verifyExists($value[0]) : false;
				$bookExist   = !empty($value[3]) ? $this->verifyExists(null, $value[3]) : false;

				if (!$authorExist) {
					array_push($arrayAuthors, [
						'firstName' => $value[1],
						'lastName' => $value[2]
					]);

					$authorCreated = $this->addAuthor(json_encode($arrayAuthors));
				}				

				if (!$bookExist && count($value[3]) > 0 && !empty(trim($value[4]))) {
					array_push($arrayBooks, [
						'title' => $value[4],
						'authorId' => $authorExist ? $value[0] : $authorCreated[0]['id']
					]);	
				}
			}
		}

		return array('books' => json_encode($arrayBooks));
	}

	/**
	* Function to verify if authors or book exists
	*/
	private function verifyExists($idAuthor, $idBook = null)
	{
		$client = new Client();
		
		$uri = $idAuthor != null ? "https://bibliapp.herokuapp.com/api/authors/". $idAuthor ."/exists" : "https://bibliapp.herokuapp.com/api/books/". $idBook ."/exists";
		$res = $client->request('GET', $uri, []);

		if ($res->getStatusCode() == self::STATUS_OK) {

			$response = json_decode($res->getBody(), true);		
			
		}

		return $response['exists'];
	}

	/**
	* Function to get all data author and books
	*/
	private function getAllDataByAuthors()
	{
		$client = new Client();
		$res = $client->request('GET', 'https://bibliapp.herokuapp.com/api/authors', []);

		$authors = "No data found";

		if ($res->getStatusCode() == self::STATUS_OK) {

			$authors = json_decode($res->getBody(), true);		
		
			foreach ($authors as $key => $author) {
				$uri = "https://bibliapp.herokuapp.com/api/authors/" . $author['id'] ."/books";
				$resBook = $client->request('GET', $uri, []);
				$books = json_decode($resBook->getBody(), true);

				$authors[$key]['books'] = $books;
			}
		}

		return $authors;
	}

	/**
	* Function to generate a exemple file
	*/
	public function generateFileExemple()
	{
		$data = $this->getAllDataByAuthors();
		$file = self::FILENAME;

		if (file_exists($file))
			unlink($file);

		$fp = fopen($file, 'a');
		fwrite($fp, "id Author|firstName|lastName|id Book|title\n");

		foreach ($data as $value) {

			if (count($value['books']) > 0) {

				foreach ($value['books'] as $book) {
					if (!empty(trim($book["title"]))) {
						fwrite($fp, 
						$value["id"] . "|" . $value["firstName"] . "|" . 
						$value["lastName"] . "|" . $book["id"] . '|' . $book["title"] . "\n");	

					} else {
						fwrite($fp, $value["id"] . "|" . $value["firstName"] . "|" . $value["lastName"] . '||' . "\n");
					}					
				}
				
			} else {
				fwrite($fp, $value["id"] . "|" . $value["firstName"] . "|" . $value["lastName"] . '||' . "\n");
			}
			
		}

		fclose($fp);

		if (file_exists($file)) {
		    header('Content-Description: File Transfer');
		    header('Content-Type: application/octet-stream');
		    header('Content-Disposition: attachment; filename="'.basename($file).'"');
		    header('Expires: 0');
		    header('Cache-Control: must-revalidate');
		    header('Pragma: public');
		    header('Content-Length: ' . filesize($file));
		    readfile($file);
		    exit;
		}
	}

	private function addAuthor($jsonAuthor)
	{
		$client = new Client();
		$res = $client->request('POST', 'https://bibliapp.herokuapp.com/api/authors', [
			'headers'  => ['content-type' => 'application/json'],
			'body' => $jsonAuthor
		]);

		return json_decode($res->getBody(), true);
	}

	public function deleteBook($id)
	{
		try {
			$client = new Client();
			$uri = 'https://bibliapp.herokuapp.com/api/books/' . $id;
			$res = $client->request('DELETE', $uri, []);

			session()->flash('message', 'Deleted book successful');

		} catch(\Exception $e) {
			session()->flash('error', 'Error delete book');
		}		

		return redirect()->route('home');
	}

	public function deleteAuthor($id)
	{
		try {
			$client = new Client();
			$uri     = 'https://bibliapp.herokuapp.com/api/authors/' . $id;
			$uriBook = 'https://bibliapp.herokuapp.com/api/authors/' . $id . '/books';
			$client->request('DELETE', $uriBook, []);
			$client->request('DELETE', $uri, []);

			session()->flash('message', 'Deleted author successful');

		} catch(\Exception $e) {
			session()->flash('error', 'Error delete author');
		}		

		return redirect()->route('home');
	}
}
